import sys
import argparse
import itertools
import torch
import torch.utils.data
import torch.nn as nn
import tqdm
import numpy as np
import matplotlib.pyplot as plt

from C_datasets import Dataset, Dataset2d, Dataset2dRandom
from C_activation import CReLU, zReLU, Mod, modReLU, C_ImagSigmoid, C_RealSigmoid, Cardioid
from C_layers import Dropout, Dropout2d, C_MaxPool2d, C_AvgPool2d, C_ConvTranspose2d

SIZE = 32
N_TRAIN = 4000
N_EVAL = 1
BATCH_SIZE = 4
DTYPE = torch.complex64

dataset = Dataset2dRandom(size=SIZE)


def build_model(device):
    model = nn.Sequential(
        nn.Conv2d(1, 4, 6, 1, 0, device=device, dtype=DTYPE),
        C_MaxPool2d(4,1),
        nn.Conv2d(4, 8, 4, 1, 0, device=device, dtype=DTYPE),
        C_MaxPool2d(4,1),
        nn.Conv2d(8, 16, 4, 1, 0, device=device, dtype=DTYPE),
        C_MaxPool2d(4,1),

        C_ConvTranspose2d(16, 8, 4, 1, device=device),
        C_ConvTranspose2d(8, 4, 4, 1, device=device),
        C_ConvTranspose2d(4, 4, 4, 1, device=device),
        C_ConvTranspose2d(4, 2, 5, 1, device=device),
        C_ConvTranspose2d(2, 1, 8, 1, device=device),

    )
    optim = torch.optim.Adam(model.parameters(), lr=5e-4)

    return model, optim


def train_model(dataset, model, optim, device):
    
    dataloader = torch.utils.data.DataLoader(dataset, batch_size=BATCH_SIZE, num_workers=2)
    it_train = iter(dataloader)

    bar = tqdm.tqdm(range(N_TRAIN), "Training iterations")
    for i in bar:
        x, y = next(it_train)
        x, y = x.to(device), y.to(device)
        x = x.reshape(BATCH_SIZE,1,SIZE,SIZE) #TODO
        # Forward
        y_pred = model(x)
        loss = ((y_pred - x).abs() ** 2).sum()

        optim.zero_grad()
        loss.backward()
        optim.step()

        bar.set_postfix({"loss":loss.item()})

def eval_model(dataset, model, device):
    model.eval()
    test_loss = 0
    with torch.no_grad():
        for (x, y) in itertools.islice(dataset, N_EVAL):
            x, y = x.to(device), y.to(device)
            x = x.reshape(1,1,SIZE,SIZE) #TODO

            # Forward
            y_pred = model(x)
            test_loss += ((y_pred - x).abs() ** 2).sum().item()
    test_loss /= N_EVAL
    test_loss = np.sqrt(test_loss)
    print(f"The loss evaluated on {N_EVAL} samples is {test_loss}")

def plot_results(dataset, model, device):
    N_cols = 3
    X, Y = np.meshgrid(range(SIZE), range(SIZE))
    Z, y = next(itertools.islice(dataset, 1))
    y_pred = model(Z.reshape(1, 1, SIZE, SIZE).to(device))
    y_pred = y_pred.detach().cpu()
    loss = ((y_pred - Z).abs() ** 2).sum().item()

    plt.figure(figsize=(30,10))


    plt.subplot(1, N_cols, 1)
    plt.pcolormesh(X, Y, Z.abs())

    plt.subplot(1, N_cols, 2)
    plt.pcolormesh(X, Y, y_pred.abs()[0,0,:,:])

    plt.subplot(1, N_cols, 3)
    plt.pcolormesh(X, Y, ((y_pred - Z).abs() ** 2)[0,0,:,:])

    plt.title(f"Loss: {loss}")


    plt.savefig("figures/test_identity.png")
    plt.show()

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--device", choices=["cpu", "cuda"], default="cpu")
    args = parser.parse_args()

    model, optim = build_model(args.device)
    train_model(dataset, model, optim, args.device)
    eval_model(dataset, model, args.device)
    plot_results(dataset, model, args.device)
