# TEST COMPLEX CNN

Test Pytorch activation functions and layers on complex valued data.

---

To activate virtual environment:
```
python3 -m venv env
source env/bin/activate
python3 -m pip install -r requirements.txt
```

## Activation functions

- [x] `nn.Sigmoid()`
Sigmoid(10+10j) = (1.0000381469726562-2.470040271873586e-05j)
- [x] `nn.Tanh()`
Tanh(10+10j) = (1+3.763441114301713e-09j)
- [ ] `nn.Relu()`
- [x] `CRelu()`
- [x] `zRelu()`
- [x] `modRelu()`
- [x] `C_imagSigmoid()`
- [x] `C_realSigmoid()`
- [x] `Cardioid()`


## Layers

- [ ] `nn.BatchNorm2d()`
- [ ] `nn.BatchNorm1d()`
- [x] `nn.Conv1d()`
- [x] `nn.Conv2d()`
- [ ] `nn.AvgPool2d()`
- [ ] `nn.MaxPool2d()`
- [x] `nn.C_MaxPool2d()`
- [x] `nn.C_AvgPool2d()`
- [ ] `nn.ConvTranspose2d()`

## Links

https://pytorch.org/docs/stable/notes/autograd.html#complex-autograd-doc

https://pytorch.org/docs/stable/complex_numbers.html#autograd

[Deep Complex Networks](https://arxiv.org/abs/1705.09792)

[A Survey of Complex-Valued Neural Networks](https://arxiv.org/abs/2101.12249)