#!/usr/bin/env python

"""
Script for demoing complex data in pytorch
"""


import sys
import argparse
import itertools
import torch
import torch.utils.data
import torch.nn as nn
import tqdm
import numpy as np
import matplotlib.pyplot as plt

from C_datasets import Dataset, Dataset2d
from C_activation import CReLU, zReLU, Mod, modReLU, C_ImagSigmoid, C_RealSigmoid, Cardioid
from C_layers import Dropout, Dropout2d, C_MaxPool2d, C_AvgPool2d, C_ConvTranspose2d

def test_data():
    dataloader = torch.utils.data.DataLoader(Dataset(), batch_size=32, num_workers=2)

    X, Y = next(iter(dataloader))
    print("Dataset 1d")
    print(f"Got an input tensor of shape {X.shape} and type {X.dtype}")

    dataloader = torch.utils.data.DataLoader(Dataset2d(), batch_size=32, num_workers=2)

    X, Y = next(iter(dataloader))
    print("Dataset 2d")
    print(f"Got an input tensor of shape {X.shape} and type {X.dtype}")

def test_activations():
    z = torch.tensor(10 + 10j, dtype=torch.complex64).reshape(1,1)
    print("Input:", z.item())
    print("Sigmoid")
    print(nn.Sigmoid()(z).item())
    print("Tanh")
    print(nn.Tanh()(z).item())

def test_linear(args):
    dataset = Dataset()
    dataloader = torch.utils.data.DataLoader(dataset, batch_size=32, num_workers=2)
    dtype = torch.complex64
    device = torch.device(args.device)
    model = nn.Sequential(
        nn.Linear(1, 128, dtype=dtype),
        Dropout(0.5),
        zReLU(),
        # nn.BatchNorm1d(32, dtype=dtype),
        # nn.Dropout(),
        nn.Linear(128, 128, dtype=dtype),
        Dropout(0.5),
        zReLU(),
        nn.Linear(128, 1, dtype=dtype),
        Mod(),
    )

    optim = torch.optim.Adam(model.parameters(), lr=3e-4)
    # Train the network
    it_train = iter(dataloader)
    n_steps = 3000
    model.train()
    for i in tqdm.tqdm(range(n_steps)):
        x, y = next(it_train)
        x, y = x.to(device), y.to(device)

        # Forward
        y_pred = model(x)
        loss = ((y_pred - y) ** 2).sum()
        sys.stdout.write(f"\r {loss}")
        sys.stdout.flush()

        optim.zero_grad()
        loss.backward()
        optim.step()

    # Evaluate
    model.eval()
    test_loss = 0
    n_samples = 1000
    with torch.no_grad():
        for (x, y) in itertools.islice(dataset, n_samples):
            x, y = x.to(device), y.to(device)

            # Forward
            y_pred = model(x)
            test_loss += ((y_pred - y) ** 2).sum().item()
    test_loss /= n_samples
    test_loss = np.sqrt(test_loss)
    print(f"The loss evaluated on {n_samples} samples is {test_loss}")

    # Display the learned function
    x = np.linspace(-1, 1)
    y = np.linspace(-1, 1)
    X, Y = np.meshgrid(x, y)
    inputs = torch.tensor(X * 1j + Y, dtype=dtype).reshape(-1, 1)
    expected = inputs.abs()

    model.eval()
    with torch.no_grad():
        outputs = model(inputs)
        print(((outputs - expected) ** 2).mean())
    Z = outputs.reshape(X.shape).numpy()
    plt.figure()
    plt.pcolormesh(X, Y, Z)
    plt.colorbar()
    plt.tight_layout()
    plt.savefig("figures/abs.png")
    plt.show()


def test_conv(args):
    # Dummy example where conv are actually doing the same operations as fc layers
    dataset = Dataset()
    dataloader = torch.utils.data.DataLoader(dataset, batch_size=32, num_workers=2)
    dtype = torch.complex64
    device = torch.device(args.device)
    model = nn.Sequential(
        nn.Conv1d(1, 128, kernel_size=1, dtype=dtype),
        zReLU(),
        # nn.BatchNorm1d(32, dtype=dtype),
        Dropout2d(),
        nn.Conv1d(128, 128, kernel_size=1, dtype=dtype),
        zReLU(),
        Dropout2d(),
        nn.Flatten(),
        nn.Linear(128, 1, dtype=dtype),
        Mod(),
    )

    optim = torch.optim.Adam(model.parameters(), lr=3e-4)
    # Train the network
    it_train = iter(dataloader)
    n_steps = 1000
    model.train()
    for i in tqdm.tqdm(range(n_steps)):
        x, y = next(it_train)
        x, y = x.to(device), y.to(device)

        # Forward
        x = x.reshape(-1, 1, 1)
        y_pred = model(x)
        loss = ((y_pred - y) ** 2).sum()
        sys.stdout.write(f"\r {loss}")
        sys.stdout.flush()

        optim.zero_grad()
        loss.backward()
        optim.step()

    # Evaluate
    model.eval()
    test_loss = 0
    n_samples = 1000
    with torch.no_grad():
        for (x, y) in itertools.islice(dataset, n_samples):
            x, y = x.to(device), y.to(device)
            x = x.reshape(-1, 1, 1)

            # Forward
            y_pred = model(x)
            test_loss += ((y_pred - y) ** 2).sum().item()
    test_loss /= n_samples
    test_loss = np.sqrt(test_loss)
    print(f"The loss evaluated on {n_samples} samples is {test_loss}")

    # Display the learned function
    x = np.linspace(-1, 1)
    y = np.linspace(-1, 1)
    X, Y = np.meshgrid(x, y)
    inputs = torch.tensor(X * 1j + Y, dtype=dtype).reshape(-1, 1, 1)
    expected = inputs.abs()

    model.eval()
    with torch.no_grad():
        outputs = model(inputs)
        print(((outputs - expected) ** 2).mean())
    Z = outputs.reshape(X.shape).numpy()
    plt.figure()
    plt.pcolormesh(X, Y, Z)
    plt.colorbar()
    plt.savefig("figures/test_conv.png")
    plt.show()

def test_conv2d(args):
    dataset = Dataset2d()
    dataloader = torch.utils.data.DataLoader(dataset, batch_size=32, num_workers=2)
    dtype = torch.complex64
    device = torch.device(args.device)
    model = nn.Sequential(
        nn.Conv2d(1, 8, kernel_size=2, dtype=dtype),
        C_RealSigmoid(),
        C_ImagSigmoid(),
        #nn.BatchNorm2d(8, dtype=dtype),
        Dropout2d(),
        nn.Conv2d(8, 16, kernel_size=2, dtype=dtype),
        C_MaxPool2d(2, 2),
        modReLU(),
        C_AvgPool2d(2, 2),
        Cardioid(),
        Dropout2d(),
        C_ConvTranspose2d(16,8,2,1),
        nn.Flatten(),
        nn.Linear(72, dataset.size, dtype=dtype),
        Mod(),
    )

    optim = torch.optim.Adam(model.parameters(), lr=3e-4)
    # Train the network
    it_train = iter(dataloader)
    n_steps = 1000
    model.train()
    for i in tqdm.tqdm(range(n_steps)):
        x, y = next(it_train)
        x, y = x.to(device), y.to(device)

        # Forward
        x = x.reshape(-1, 1, dataset.size, dataset.size)
        y_pred = model(x)
        loss = ((y_pred - y) ** 2).sum()
        sys.stdout.write(f"\r {loss}")
        sys.stdout.flush()

        optim.zero_grad()
        loss.backward()
        optim.step()

    # Evaluate
    model.eval()
    test_loss = 0
    n_samples = 100
    with torch.no_grad():
        for (x, y) in itertools.islice(dataset, n_samples):
            x, y = x.to(device), y.to(device)
            x = x.reshape(-1, 1, dataset.size, dataset.size)

            # Forward
            y_pred = model(x)
            test_loss += ((y_pred - y) ** 2).sum().item()
    test_loss /= n_samples
    test_loss = np.sqrt(test_loss)
    print(f"The loss evaluated on {n_samples} samples is {test_loss}")

    # Plot results
    X, Y = np.meshgrid(range(dataset.size), range(dataset.size))
    N_rows = N_cols = 3

    plt.figure(figsize=(14,14))
    for i in range(N_rows):
        for j in range(N_cols):

            Z, y = next(itertools.islice(dataset, N_cols*N_rows+1))
            y_pred = model(Z.reshape(-1, 1, dataset.size, dataset.size))
            ind = i*N_rows + j
            plt.pcolormesh(X, Y, Z.abs())
            plt.title(f"predicted: {torch.argmax(y_pred).item()} -- true: {torch.argmax(y).item()}")
            plt.subplot(N_rows, N_cols, ind+1)
    plt.savefig("figures/test_2d.png")
    plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--device", choices=["cpu", "cuda"], default="cpu")
    args = parser.parse_args()

    test_data()
    test_activations()
    test_conv2d(args)
    test_linear(args)
    test_conv(args)
