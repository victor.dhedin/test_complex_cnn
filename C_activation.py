import torch
import torch.nn as nn

class CReLU(nn.Module):
    def __init__(self):
        super().__init__()
        self.relu = nn.ReLU()

    def forward(self, z):
        return self.relu(z.real) + self.relu(z.imag) * 1j

class zReLU(nn.Module):
    def forward(self, z):
        pos_real = z.real > 0
        pos_img = z.imag > 0
        return z * pos_real * pos_img

class Mod(nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, z):
        return torch.abs(z)

class modReLU(nn.Module):
    def __init__(self) -> None:
        super().__init__()
        self.b = torch.nn.Parameter(torch.tensor(0., dtype=torch.float), True)
    
    def forward(self, z):
        return nn.functional.relu(z.abs() + self.b)*torch.exp(1j*z.angle())

class C_RealSigmoid(nn.Module):
    '''
    Sigmoid on real part.
    '''
    def __init__(self) -> None:
        super().__init__()

    def forward(self, z):
        return torch.sigmoid(z.real) + 1j*z.imag

class C_ImagSigmoid(nn.Module):
    '''
    Sigmoid on real part.
    '''
    def __init__(self) -> None:
        super().__init__()

    def forward(self, z):
        return z.real + 1j*torch.sigmoid(z.imag)

class Cardioid(nn.Module):
    '''
    Cardioid activation function.
    '''
    def __init__(self) -> None:
        super().__init__()
    
    def forward(self, z):
        return 1/2*(1+torch.cos(z.angle()))*z