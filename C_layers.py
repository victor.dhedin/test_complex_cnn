import torch
import torch.nn as nn
import torch.functional as F

class Dropout(nn.Module):
    def __init__(self, p=0.5):
        super().__init__()
        self.p = p

    def forward(self, z):
        mask = torch.nn.functional.dropout(
            torch.ones(z.shape), self.p, training=self.training
        )
        return mask * z

class Dropout2d(nn.Module):
    def __init__(self, p=0.5, device="cpu"):
        super().__init__()
        self.device = torch.device(device)
        self.p = p

    def forward(self, z):
        mask = torch.nn.functional.dropout2d(
            torch.ones(z.shape), self.p, training=self.training
        ).to(self.device)
        return mask * z

class C_MaxPool2d(nn.Module):
    '''
    Implementation of torch.nn.MaxPool2d for complex numbers.
    Apply MaxPool2d on the module of the input.
    Returns complex values associated to the MaxPool indices.
    '''
    def __init__(self, kernel_size, stride=None, padding=0, dilation=1, ceil_mode=False, return_indices=False) -> None:
        super().__init__()
        self.return_indices = return_indices
        self.m = torch.nn.MaxPool2d(kernel_size,
                                    stride,
                                    padding,
                                    dilation,
                                    ceil_mode=ceil_mode,
                                    return_indices=True)


    def forward(self, z: torch.Tensor):
        _, indices = self.m(torch.abs(z))
        
        if self.return_indices:
            return z.flatten()[indices], indices
        else :
            return z.flatten()[indices]

class C_AvgPool2d(nn.Module):
    '''
    Implementation of torch.nn.AvgPool2d for complex numbers.
    Apply AvgPool2d on the real and imaginary part.
    Returns complex values associated to the AvgPool2dresults.
    '''
    def __init__(self, kernel_size, stride=None, padding=0, ceil_mode=False, count_include_pad=True, divisor_override=None) -> None:
        super().__init__()
        if type(kernel_size) == int:
            self.kernel_size = [kernel_size]*2 + [1]
        elif type(kernel_size) == tuple:
            if len(kernel_size) < 3:
                self.kernel_size = [kernel_size] + [1]
            else :
                self.kernel_size = kernel_size

        if type(stride) == int:
            self.stride = [stride]*2 + [1]
        elif type(stride) == tuple:
            if len(stride) < 3:
                self.stride = [stride] + [1]
            else :
                self.stride = stride

        self.m = torch.nn.AvgPool3d(self.kernel_size,
                                    self.stride,
                                    padding,
                                    ceil_mode,
                                    count_include_pad,
                                    divisor_override)

    def forward(self, z: torch.Tensor):
        return torch.view_as_complex(self.m(torch.view_as_real(z)))

class C_ConvTranspose2d(nn.Module):
    '''
    Implementation of torch.nn.Conv2dTranspose for complex numbers.
    Apply Conv2dTranspose on real and imaginary part of the complex number.
    '''
    def __init__(self,
                 in_channels,
                 out_channels,
                 kernel_size,
                 stride=1,
                 padding=0,
                 output_padding=0,
                 groups=1,
                 bias=True,
                 dilation=1,
                 padding_mode='zeros',
                 device=None,
                 dtype=None) -> None:
        super().__init__()

        self.m_real = torch.nn.ConvTranspose2d( in_channels,
                                                out_channels,
                                                kernel_size,
                                                stride,
                                                padding,
                                                output_padding,
                                                groups,
                                                bias,
                                                dilation,
                                                padding_mode,
                                                device,
                                                dtype)

        self.m_imag = torch.nn.ConvTranspose2d( in_channels,
                                                out_channels,
                                                kernel_size,
                                                stride,
                                                padding,
                                                output_padding,
                                                groups,
                                                bias,
                                                dilation,
                                                padding_mode,
                                                device,
                                                dtype)

    def forward(self, z: torch.Tensor):
        return torch.view_as_complex(torch.concatenate((torch.unsqueeze(self.m_real(z.real) - self.m_imag(z.imag), -1) ,
                                                        torch.unsqueeze(self.m_real(z.imag) + self.m_imag(z.real), -1)),
                                                        axis=-1)
                                    )
if __name__ == "__main__":
    A = torch.tensor([[[1,1],[1,1],[1,1]],[[2,2],[2,2],[2,2]],[[3,3],[3,3],[3,3]]], dtype=torch.float32)
    input = torch.view_as_complex(A)

    print("Input:")
    print(input)

    print("\nConv2d:")
    m = torch.nn.Conv2d(1,1,2,1, dtype=torch.complex64)
    print(m(input.reshape(1,1,3,3)))

    print("\nC_MaxPool2d:")
    m = C_MaxPool2d(2,1)
    print(m(input.reshape(1,1,3,3)))

    print("\nC_AvgPool2d:")
    m = C_AvgPool2d(2,1)
    print(m(input.reshape(1,1,3,3)))

    print("\nC_ConvTranspose2d:")
    m = C_ConvTranspose2d(1,1,2,1)
    print(m(input.reshape(1,1,3,3)))

