import torch
import torch.nn as nn

class Dataset(torch.utils.data.IterableDataset):
    def __init__(self):
        super().__init__()

    def __next__(self):
        x = (2 * torch.rand(1) - 1.0) + (2 * torch.rand(1) - 1) * 1j
        y = x.abs()
        return x, y

    def __iter__(self):
        return self

class Dataset2d(torch.utils.data.IterableDataset):
    def __init__(self, size=10):
        super().__init__()
        self.size = size

    def __next__(self):
        x = torch.zeros((self.size, self.size), dtype=torch.complex64)
        y = torch.randint(self.size-1, (1,))
        x[y,:] = torch.ones((1, self.size), dtype=torch.complex64)
        y = nn.functional.one_hot(y, num_classes = self.size)
        return x, y

    def __iter__(self):
        return self

class Dataset2dRandom(torch.utils.data.IterableDataset):
    def __init__(self, size=10):
        super().__init__()
        self.size = size

    def __next__(self):
        x = torch.linspace(0, 1, self.size, dtype=torch.complex64).repeat(self.size).reshape(self.size, self.size)
        #x = torch.randn((self.size, self.size), dtype=torch.complex64)
        y = torch.randint(self.size-1, (1,))
        return x, y

    def __iter__(self):
        return self
